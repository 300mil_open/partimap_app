function key(d){
	let r = Object.keys(d)[0];
	return r;
}
	var viewer = new Vue({
		el: '#app',
		data: function(){
			var is_landscape;
			if (window.innerHeight<window.innerWidth){
				is_landscape =  true
			}else{
				is_landscape =  false
			}

			return {
				waiting:false,
				db:null, 

				uid:null, // el id de usuario
				user:{}, // los meta del usuario
				user_name: null,
				user_stats:{},
				logged:false,
				current:'landing-home',

				is_landscape: is_landscape,
				is_ready:false,

				phone:null,
				password:null,
				recaptchaVerifier:null,
				confirmationResult:null,

				log_name:'',
				log_age:null, 
				log_place:null,
				log_sex:null,

				log_time_hh:null, 
				log_time_nh:null, 
				log_time_ci:null, 

				log_current:'log-0',

				log_places_options: [
					{ text: 'Mukuru', value: 'Mukuru' },
					{ text: 'Kibera', value: 'Kibera' },
					{ text: 'Kariobangi', value: 'Kariobangi' },
					{ text: 'Korogocho', value: 'Korogocho' },
					{ text: 'Waruku', value: 'Waruku' },
					{ text: 'Mathare', value: 'Mathare' },
					{ text: 'Kahawa Soweto', value: 'Kahawa Soweto' },
					{ text: 'Other places in Nairobi', value: 'Other places in Nairobi' },
					{ text: 'Other countries', value: 'Other countries' },
				  ],

				log_education:null,
				log_education_options:[
					{ text: 'Primary School', value: 'Primary School' },
					{ text: 'High School', value: 'High School' },
					{ text: 'Technical Certificate', value: 'Technical Certificate' },
					{ text: 'University / Higher Education', value: 'University / Higher Education' },
					{ text: 'None', value: 'None' },
				],

				c_sessions_open: null,
				c_sessions_private: null,
				c_sessions_private_active : null,
				c_sessions_active: null, 

				t_folder:'',
				t_photo1:false,
				t_photo2:false,
				t_ready:false,
				gameover: false,
				limit_votes:20,
				limit:null,

				selection:null,

				localwarning:null,
				globalwarning:null,
				type_of_session:null,
			}
		},

        methods: {
			getwarnings: function(){

				this.db.ref('globalwarning').once('value', d=>{
					let startwarning = d.val();
					this.db.ref('globalwarning').on('value', d=>{
						let gl= d.val();
						if (gl != startwarning){
							startwarning=null;
							this.globalwarning =gl;
						}
					})
				})

				this.db.ref('messages/'+this.uid).once('value', d=>{
					let startlocal = d.val();
					this.db.ref('messages/'+this.uid).on('value', d=>{
						let glocal= d.val();
						if (glocal != startlocal){
							startlocal=null;
							this.localwarning = glocal;
						}
					})
				})
			},

			waitingRoom: function(){
				this.current='waitingroom';

				// set waiting status
				this.db.ref('c_waitingroom').orderByChild('uid').equalTo(this.uid).once('value', data=>{ //on ore once??
					if (data.val() == null){
						this.db.ref('c_mainroom').orderByChild('uid').equalTo(this.uid).once('value', dd=>{
							if (dd.val() == null){
								let ts = new Date().getTime();
								this.db.ref('c_waitingroom').push({'uid':this.uid, 'ts': ts, 'name': this.user_name})
							}else{
								console.log('yet playing...')
							}
						 })
						
					}else{
						console.log('yet waiting...')
					}
				})

				// ask permision
				this.db.ref('c_mainroom')
					.orderByChild('uid')
					.equalTo(this.uid)
					.on('value', (data) => {
						if( data.val()!=null ){
							this.current='welcome'
					 }
				})
			},


			vote: function(photo){
				
				this.t_ready  = false;
				this.selection = null;

				if (photo==0){
					wins = this.t_photo1;
					loose = this.t_photo2;
				}
				else{
					loose = this.t_photo1;
					wins = this.t_photo2;
				}

				var currentDate = new Date().toLocaleDateString('es-es');
				var ts = new Date().getTime();


				//add vote 
				this.db.ref('votes').push({'uid':this.uid, 'wins':wins, 'loose':loose, 'date':currentDate, 'ts':ts, 'session': this.c_sessions_active.id})


				//add vote by user
				vid_user = this.t_photo1+'_'+this.t_photo2+'_'+this.uid;
				this.db.ref('votes_by_user').push({'vid_user':vid_user,  'date':currentDate, });


				//add vote_sequence solamente si la secuencia no es random 
				console.log('>>>', this.c_sessions_active, 'c_sequences/'+this.c_sessions_active.seq, this.t_photo1+'_'+this.t_photo2 )
				if (this.c_sessions_active.type=='seq'){
					this.db.ref('c_sequences/'+this.c_sessions_active.seq).orderByChild('vid').equalTo(this.t_photo1+'_'+this.t_photo2)
						.once('value').then(data => {
							d = data.val()
							d[Object.keys(d)[0]]['votes'] = d[Object.keys(d)[0]]['votes']+1
							this.db.ref('c_sequences/'+this.c_sessions_active.seq).update(d)
							console.log('updated sequence :' , d[Object.keys(d)[0]]['votes'], Object.keys(d)[0] )
						})
				}
				
				//add votes to each user
				this.db.ref('users').orderByChild('uid').equalTo(this.uid).get().then(data=>{
					d =  data.val()
					votes = d[key(d)]['votes']
					this.db.ref('users/'+key(d)).update({'votes':votes+1, 'l_ts': new Date().getTime()})
				})
				
				// stats  check
				this.db.ref('votes_stats').get().then(d=>{
					if (d.val() == null || d.val()['votes_n'] == undefined ){
						v = {'votes_n':1}
					}else{
						v = {'votes_n': d.val()['votes_n']+1}  
					}
					this.db.ref('votes_stats').set(v);
				})

				// agregar votos de sesion
				if (this.type_of_session == 'open'){
					this.db.ref('c_sessions_open/'+this.c_sessions_active.id).get().then(d=>{

						if (d.val() && d.val()['votes'] == undefined){
							v = {'votes':1}
						}else{
							v = {'votes': d.val()['votes']+1}  
						}

						this.db.ref('c_sessions_open/'+this.c_sessions_active.id).update(v);
					})
				}else if(this.type_of_session == 'private'){
					this.db.ref('c_sessions_private/'+this.c_sessions_active.id).get().then(d=>{

						if (d.val() && d.val()['votes'] == undefined){
							v = {'votes':1}
						}else{
							v = {'votes': d.val()['votes']+1}  
						}

						console.log('update c_sessions_private votes' ,this.c_sessions_active.id, v )
						this.db.ref('c_sessions_private/'+this.c_sessions_active.id).update(v);
					})
				}

				// prepare next match
				this.prepareMatch();
				
			},

			selectMatch: function(sid, typ){

				this.type_of_session = typ;
				if (typ=='open'){
					this.c_sessions_active = this.c_sessions_open[sid];
					this.prepareMatch();
				}else if(typ=='private'){
					this.c_sessions_active = this.c_sessions_private[sid];
					this.limit = this.c_sessions_active['limit']

					this.db.ref('limit').on('value', d=>{
						let v = d.val()
						if (v > this.c_sessions_active['limit']){
							this.limit = v;
							//el limite solamente se augmenta no se reduce
						}
					})

					this.waitingRoom();
				}

			},

			prepareMatch: function(){

				this.gameover=false;
				this.current='gym';

				//var tt = this.c_sessions_private[this.c_sessions_private_active]

				let tt = this.c_sessions_active;
				this.t_folder = tt['folder'];

				/////// coger la sesion de netre la spublicas o las provadas!!
				if (tt['type'] == 'random_subset'){
					this.t_ready  = false;
					this.t_photo1 = false;
					this.t_photo2 = false;

					let base = tt.seq; // thi snumber to be update according the final dataset size
					let p1 = Math.floor(Math.random()*base)+1;
					this.db.ref('c_sequences_x/'+tt['subset']+'/'+p1).once('value', d=>{
						this.t_photo1 = d.val();
						let c = true;
						while (c){
							let candidate = Math.floor(Math.random()*(base))+1;
							if (candidate != p1){
								p2 = candidate;
								c= false;
								this.db.ref('c_sequences_x/'+tt['subset']+'/'+p2).once('value', d=>{
									this.t_photo2 = d.val();
									this.t_ready = true;
								})
							}
						}
					})
				}
				else if (tt['type'] == 'random'){
					this.t_ready  = false;
					this.t_photo1 = false;
					this.t_photo2 = false;

					let base = tt.seq; // thi snumber to be update according the final dataset size
					this.t_photo1 = Math.floor(Math.random()*base)+1;
					let c = true;
					while (c){
						let candidate = Math.floor(Math.random()*(base))+1;
						if (candidate != this.t_photo1){
							this.t_photo2 = candidate;
							c= false;
						}
					}
					this.t_ready = true;

				}else if(tt['type'] == 'seq'){
					this.t_ready  = false;
					this.t_photo1 = false;
					this.t_photo2 = false; 
				
					this.db.ref('c_sequences/'+tt['seq'])
						.orderByChild('votes')
						.endAt(this.limit-1)
						.limitToFirst(50) /////esto lo reducimos??
						.once('value')
						.then((d)=>{
							if (d.val() != null){
								
								let vids = Object.keys(d.val());
								let nvids = vids.length;
								let n = Math.floor(Math.random()*(nvids));
								let vid = d.val()[vids[n]]['vid'];

								this.t_photo1 = vid.split('_')[0];
								this.t_photo2 = vid.split('_')[1];
								this.t_ready = true;

								console.log('--', this.limit, vids[n] ,nvids, vid)
							}
							else{
								this.gameover=true;
							}
						})
				}
			},

			check_if_voted: async function(vid_user){
				await this.db.ref('votes_by_user').orderByChild('vid_user').equalTo(vid_user).once('value', data => {
					console.log(data.val())
					if (data.val() == null){
						return false // la foto es buena
					}else{
						return true // la foto es mala
					}
				})
			},

			reportWindowSize: function(){
				//
				if (window.innerHeight<window.innerWidth){
					this.is_landscape =  true;
					if (this.is_ready && this.current=='landing-home'){
						this.current='landing-welcome';
					}
				}else{
					this.is_landscape =  false;
				}
			},		

            enrichUserData: function(){
				//the place
				//$.getJSON('http://www.geoplugin.net/json.gp?', (data) => {
				fetch('http://www.geoplugin.net/json.gp?').then( (r) => 
					r.json() 
				).then( data=>{
					this.user['g_latitude'] = data['geoplugin_latitude'] || null;
					this.user['g_longitude'] = data['geoplugin_longitude'] || null;
					this.user['g_accuracy'] = data['geoplugin_locationAccuracyRadius'] || null;
					this.user['g_city'] = data['geoplugin_city'] || null;
					this.user['g_country'] = data['geoplugin_countryName'] || null;
					this.user['g_ip'] = data['geoplugin_request'] || null;
				})

				// the phone
				var md = new MobileDetect(window.navigator.userAgent);
				this.user['d_mobile'] = md.mobile() || null;
				this.user['d_phone'] = md.phone() || null;
				this.user['d_tablet'] = md.tablet() || null;
				this.user['d_useragent'] = window.navigator.userAgent || null;
				this.user['d_version'] = md.versionStr('Build') || null;

				// the date
				var currentDate = new Date();
				this.user['t_ts'] = currentDate.getTime();
				this.user['t_day'] = currentDate.getDate();
				this.user['t_month'] = currentDate.getMonth() + 1;
				this.user['t_year'] = currentDate.getFullYear();

			},

			logInit: function(){
				console.log('loginit')
				this.logged=false;
				firebase.auth().signOut().then(()=>{
					this.enrichUserData();
					this.user={}
				}).catch((error)=>{});
			},

			getActiveSessions: function(){
				this.db.ref('c_sessions_open').get().then((data)=> {this.c_sessions_open = data.val()});
				this.db.ref('c_sessions_private').get().then((data)=> {this.c_sessions_private = data.val()});
				this.db.ref("c_sessions_private_active").on('value', (data) => {
					this.c_sessions_private_active  = data.val()
				})
			},

			logUserPhone: function(){

				this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
					'size': 'invisible',
					'callback': (response) => {}
				});

				firebase.auth().signInWithPhoneNumber(this.phone, this.recaptchaVerifier)
					.then((confirmationResult) => {
						console.log('!!!: message sent ', confirmationResult)
						this.log_current = 'log-confirm';
						this.confirmationResult = confirmationResult;
						
					}).catch((error) => {
						console.log('?', error)
					});
	
			},

			logConfirm: function(){
				this.confirmationResult.confirm(this.password).then((result) => {
					this.registerUser(result.user.uid)
				}).catch((error) => {
					
				});
			},

			logUser: function () {
				//bloquear la interaccion....
				this.waiting=true;
				console.log('login')
				firebase.auth().signInAnonymously()
					.then((s) => {
						this.registerUser(s.user.uid)
						
					}).catch(function (error) {
						var errorCode = error.code;
						var errorMessage = error.message;
						console.log(errorCode);
						console.log(errorMessage);
					});

				//guardar estadisticas
			},

			registerUser: function(uid){

				this.uid = uid;
				console.log(this.uid)
	
				this.waiting=false;
	
				this.user['uid'] = this.uid;
				this.user['u_name'] = this.log_name;
				this.user['u_sex'] = this.log_sex;
				this.user['u_age'] = this.log_age;
				this.user['u_place'] = this.log_place;
				this.user['u_phone'] = this.phone;
	
				this.user['u_studies'] = this.log_education;
				this.user['u_time_in_household'] = this.log_time_hh;
				this.user['u_time_in_neighborhood'] = this.log_time_nh;
				this.user['u_time_in_city'] = this.log_time_ci;
	
				this.user['votes'] = 0;
				this.user['l_ts'] = new Date().getTime();
	
				this.db.ref('users').push(this.user) 
				
				this.db.ref("users_count").once('value', (data) => {
					d = data.val()
					if (d == null) {
						d = {
							'total_users': 0
						}
					}
					n = d['total_users'] + 1;
					de = {
						'total_users': n
					}
					firebase.database().ref("users_count").update(de)
				})
			},

		},


		beforeMount: function(){},
        mounted: function () {

			setTimeout(()=>{
				this.is_ready=true;
				if (this.is_landscape && this.current=='landing-home'){
					this.current='landing-welcome';
				}
			},1500);

			window.onresize = this.reportWindowSize;
			window.onorientationchange = this.reportWindowSize;

			/*------------------*/

			// console.log('uid:',firebase.auth().getUid());
			this.db = firebase
				.initializeApp(firebaseConfig)
				.database();

			firebase.auth().languageCode = 'en';
			firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); //NONE



			firebase.auth().onAuthStateChanged((user) => {
				if (user) {
					console.log('User is signed in.', user.uid)
					this.uid = user.uid;
					this.logged=true;
					// this.loadUserData(this.uid);

					this.db.ref('users').get().then( (data) => {
						console.log(data)
					})
					
					this.db.ref('users').orderByChild('uid').equalTo(this.uid).limitToFirst(1).get().then( (data) => {
						k = Object.keys(data.val())[0] 
						this.user = data.val()[k];
						this.user_name = data.val()[k]['u_name'];

						this.getwarnings()
					})

					this.getActiveSessions()
				} else {

					console.log('No user is signed in.')
					this.enrichUserData();
				}
			})
        }
    })

