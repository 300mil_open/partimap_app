
function key(d){
	//entrar con d.val()
	let r = Object.keys(d)[0];
	return r;
}

function val(d){
    return d.val()[key(d.val())]
}

console.log('launching...')

var viewer = new Vue({
    el: '#app',
    mixins: [appdb],

    data:{
        waiting_room:[],
        main_room:[],
        users:{},
        tds_list:[
            // { text: 'One', value: 'A' },
            // { text: 'Two', value: 'B' },
            // { text: 'Three', value: 'C' }
        ],
        tds_active:null,
        tds_data: null,

        globalwarning:null,
        globalwarning_list:[],

        sendusermessage:null, 
        usermessage:null, 
        uid:null,
        uid_name:null, 
        limit : null,
    },

    methods: {

        writtemessage: function(uid, uid_name){
            this.uid = uid;
            this.uid_name = uid_name;
            this.sendusermessage=true
        },

        sendmessage2user(){
            if (this.usermessage && this.usermessage.length>0){
                this.db.ref('messages/'+this.uid).set(this.usermessage).then(d=>{
                    this.uid = null;
                    this.uid_name = null;
                    this.usermessage=null;
                    this.sendusermessage=null;
                })
            }else{
                this.uid = null;
                this.uid_name = null;
                this.usermessage=null;
                this.sendusermessage=null;
            }
        },

        sendGlobalWarning: function(){
            this.globalwarning_list.push(this.globalwarning)
            this.db.ref('globalwarning').set(this.globalwarning).then(d=>{
                this.globalwarning =null;
            })
        },

        listen_uid: function(uid){
            this.db.ref('users').orderByChild('uid').equalTo(uid).on('value', d =>{
                if (d.val() !=null){
                    this.users[uid]['votes'] = val(d)['votes']
                    this.users[uid]['l_ts'] = val(d)['l_ts']
                    this.$forceUpdate() // no se porque sin esto no renderiza bien
                }
            })
        },

        listen_main_room: function(){
            // solamente una vez al entrar
            this.db.ref('c_mainroom').once('value', d=>{
                let dd = d.val();
                for (let e in dd){
                    console.log(dd[e]['uid'])
                    this.users[dd[e]['uid']] ={}
                    this.listen_uid(dd[e]['uid'])
                }
            })
        },

        add_user: function(uid, k, i){
            this.db.ref('c_mainroom').orderByChild('uid').equalTo(uid).once('value', data=>{
                if(data.val() == null){

                    this.db.ref('c_mainroom').push(i);
                    this.db.ref('c_waitingroom/'+k).remove();

                    //update this.users
                    this.users[uid] = i;

                    //get users stats??
                    this.listen_uid(uid)
                    //create listeners form this.users
                }
            })
        },

        remove_user: function(uid, k, i){
            console.log('remove: ', k)
            this.db.ref('c_waitingroom/'+k).remove()
        },

        move_to_waitingroom(uid, k,i){
            this.db.ref('c_waitingroom').push(i);
            this.db.ref('c_mainroom/'+k).remove()
        },
        update: function(){
            this.$forceUpdate()
        },
        controlRoom: function(){
            this.listen_main_room();

            this.db.ref("c_sessions_private_active").set(null)

            this.db.ref('c_sessions_private').on('value', data=>{
                d = []
                for (let i in data.val()){
                    d.push( data.val()[i] )
                }
                this.tds_list = d
                // this.$forceUpdate()
            })

            this.db.ref('c_mainroom').on('value', data=>{
                this.main_room = data.val();
            })

            this.db.ref('c_waitingroom').on('value', data=>{
                this.waiting_room = data.val();
            })
        }
    },

    watch:{
        tds_active: function(){
            this.db.ref('c_sessions_private_active').set(this.tds_active);
            this.db.ref('c_sessions_private/'+this.tds_active).once('value', d=>{
                this.tds_data = d.val();
                this.limit = d.val()['limit'] || -1;
                this.db.ref('limit').set(this.limit);
            })
        },

        limit: function(){
            this.db.ref('limit').set( parseInt(this.limit) );
        }
    },

    mounted: function () {

        setInterval(this.update, 1000);

        this.db = firebase
            .initializeApp(firebaseConfig)
            .database();

        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); 
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                    this.controlRoom();
            } else {
                firebase.auth().signInAnonymously().then((s) => {
                    
                    user={}
                    user['uid'] = s.user.uid;
                    user['u_name'] = 'MASTER';
                    user['u_sex'] = null;
                    user['u_age'] = null;
                    user['u_place'] =null;
                    user['votes'] = 0;
                    user['l_ts'] = new Date().getTime();

                    this.db.ref('users').push(user);
                    this.controlRoom();
                })
            }
        })
    }
})

/*
https://firebase.google.com/docs/auth/admin/manage-users#node.js



*/