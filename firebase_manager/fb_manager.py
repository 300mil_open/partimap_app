import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import json
import sqlite3
import pandas as pd

#######################################

cred = credentials.Certificate('credentials/partimap-1ba97-f019f6b9f275.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://partimap-1ba97-default-rtdb.europe-west1.firebasedatabase.app'
})


#######################################

def drop():
    '''
    drop the entire database
    :return:
    '''
    root = db.reference()
    root.delete()
    return


#######################################


def addSequence(path, name):
    with open(path, 'r') as f:
        fi = json.load(f)

    ds = {}
    print(name, len(fi))
    for i, f in enumerate(fi):
        ds[str(i).zfill(10)]= f

    db.reference('c_sequences/' + name).delete()
    r = db.reference('c_sequences/'+name).set(ds)
    print(r)
    return

path = '../site/datasets/seq1.json'
addSequence(path, 'seq1')
path = '../site/datasets/seq1.json'
addSequence(path, 'seq2')
path = '../site/datasets/seq1.json'
addSequence(path, 'seq3')


#######################################

def addSequence_xtract(path,name):
    with open(path, 'r') as f:
        fi = json.load(f)

    db.reference('c_sequences_x/' + name).delete()
    r = db.reference('c_sequences_x/'+name).set(fi)
    print(r)
    return

addSequence_xtract('site/datasets/sat_extract.json', 'satx1')


#######################################


def addSessions():
    '''
    add sessions for dev purposes

    '''
   
    c_sessions_open={
        'o1': {'id': 'o1',
               'folder': 'satelite',
               'name':  'small Pablo',
               'description': 'random training for an small part of dataset of satelital imagery',
               'type':'random_subset',
               'subset':'satx1',
               'seq': 77,
               'votes':0,
               'num_tests':(77*77-77)/2,
               'total_photos':77,
               'limit': -1
               },

        'o2': {'id': 'o2',
               'folder': 'streetview',
               'name': 'streetview',
               'description': 'random training for the streetview dataset',
               'type': 'random',
               'seq': 96,
               'votes': 0,
               'num_tests': (96 * 96 - 96) / 2,
               'total_photos': 96,
               'limit': -1
               },
    }

    ref = db.reference('c_sessions_open')
    ref.delete()
    ref.set(c_sessions_open)


    c_sessions_private = {
        'p1': {'id': 'p1',
               'folder': 'satelite',
               'name': 'small Angela',
               'description': 'small satelital training using a sequence',
               'type':   'seq',
               'seq': 'seq2',
               'votes':0,
               'num_tests':123 ,
               'total_photos':0,
               'limit':20
               },

        'p2': {'id': 'p2',
               'folder': 'satelite',
               'name': 'big Angela',
               'description': 'big satelital training using a sequence (only 1 vote bd)',
               'type': 'seq',
               'seq': 'seq3',
               'votes': 0,
               'num_tests': 64512,
               'total_photos': 2023,
               'limit': 1
               },

        'p3': {'id': 'p3',
               'folder': 'satelite',
               'name':  'big Pablo',
               'description': 'big random training for the whole dataset of satelital imagery',
               'type':'random',
               'seq': 2023,
               'votes':0,
               'num_tests':2045253,
               'total_photos':2023,
               'limit': -1
               },

        'p4': {'id': 'p4',
               'folder': 'satelite',
               'name':  'small Pablo',
               'description': 'random training for an small part of dataset of satelital imagery',
               'type':'random_subset',
               'subset':'satx1',
               'seq': 77,
               'votes':0,
               'num_tests':(77*77-77)/2,
               'total_photos':77,
               'limit': -1
               },

        'p5': {'id': 'p5',
               'folder': 'streetview',
               'name': 'streetview',
               'description': 'random training for the streetview dataset',
               'type': 'random',
               'seq': 96,
               'votes': 0,
               'num_tests': (96 * 96 - 96) / 2,
               'total_photos': 96,
               'limit': -1
               },
    }

    ref = db.reference('c_sessions_private')
    ref.delete()
    ref.set(c_sessions_private)

    return


def addSessions_v2():

    c_sessions_private = {
        'p1b': {'id': 'p1b',
               'folder': 'satelite',
               'name': 'small Angela 2',
               'description': 'v2 small satelital training using a sequence',
               'type': 'seq',
               'seq': 'seq2b',
               'votes': 0,
               'num_tests': 123,
               'total_photos': 0,
               'limit': 20
               },

        'p2b': {'id': 'p2b',
               'folder': 'satelite',
               'name': 'big Angela 2',
               'description': 'v2 big satelital training using a sequence (only 1 vote bd)',
               'type': 'seq',
               'seq': 'seq3b',
               'votes': 0,
               'num_tests': 64512,
               'total_photos': 2023,
               'limit': 1
               }
    }
    ref = db.reference('c_sessions_private').get()

    z = {**c_sessions_private, **ref}

    print(json.dumps(z, indent=4))
    ref = db.reference('c_sessions_private')
    ref.set(z)
    return

addSessions_v2()

#########################################################################################################################

