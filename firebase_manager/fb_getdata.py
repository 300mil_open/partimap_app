import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import json
import sqlite3
import pandas as pd
'''
examples :  
https://firebase.google.com/docs/database/admin/start
'''

## LOG IN ##############################################################################################################

cred = credentials.Certificate('credentials/partimap-1ba97-f019f6b9f275.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://partimap-1ba97-default-rtdb.europe-west1.firebasedatabase.app'
})

########################################################################################################################

def getVotes(path, if_exists='replace'):
    vv = db.reference('votes').get()
    votes =[]
    if vv != None:
        for v in vv:
            votes.append(vv[v])

        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        print(df)

        df.to_sql('votes', con = con, if_exists=if_exists)
        print('downloaded and stored :', len(vv))
    return votes


def getVotesByUser(path, uid, if_exists='replace'):
    vv = db.reference('votes').order_by_child('uid').equal_to(uid).get()
    votes =[]
    if vv != None:
        for v in vv:
            votes.append(vv[v])

        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        print(df)

        df.to_sql('votes_'+uid, con = con, if_exists=if_exists)
        print('downloaded and stored :', len(vv))
    return votes


def getUsers(path, if_exists='replace'):
    vv = db.reference('users').get()
    votes =[]
    if vv != None:
        for v in vv:
            votes.append(vv[v])

        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        print(df)

        df.to_sql('users', con = con, if_exists=if_exists)
        print('downloaded and stored :', len(vv))
    return votes

def getSessions(path, if_exists='replace'):
    ####
    vv = db.reference('c_sessions_open').get()
    votes =[]
    if vv != None:
        for v in vv:
            votes.append(vv[v])
        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        print(df)
        df.to_sql('sessions_open', con = con, if_exists=if_exists)

    ####
    vv = db.reference('c_sessions_private').get()
    votes = []
    if vv != None:
        for v in vv:
            votes.append(vv[v])

        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        print(df)
        df.to_sql('sessions_private', con=con, if_exists=if_exists)


    print('downloaded and stored :', len(vv))
    return votes

def getSequences(path, seq, if_exists='replace'):
    ####

    vv = db.reference('c_sequences/'+seq).get()
    votes = []
    if vv != None:
        for v in vv:
            votes.append(vv[v])

        df = pd.DataFrame(votes)
        con = sqlite3.connect(path)
        df.to_sql(seq, con=con, if_exists=if_exists)


    print('downloaded and stored :', len(vv))
    return votes


def getItemSeq(seq, v):
    vv = db.reference('c_sequences/' + seq).order_by_child('vid').equal_to(v).limit_to_first(1).get()
    print(vv)
    return


def preparematch():
    r = db.reference('c_sequences/seq3b')\
        .order_by_child('votes').end_at(20)\
        .limit_to_first(50).get()

    print(r)
    print(len(r))
    return

def resetSequence():
    '''

    '''
    return

############################################

d = '2022_07_22'
db_out = 'bulk_%s.sqlite' %d

getUsers(db_out)
getVotes(db_out)
getSessions(db_out)
getSequences(db_out, 'seq2b')
getSequences(db_out, 'seq3b')


