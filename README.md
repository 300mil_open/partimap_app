# Partimap APP

Here you will find the code that has been used to develeope the app used in the project partimap and all the other codes needed to make it work. 


If you want to know more about the Partimap project read the paper:

[AI perceives like a local: predicting citizen deprivation perception using satellite imagery](https://www.nature.com/articles/s42949-024-00156-x)

## 

The app stores the data in a Firebase service. There is stored the information about the users but also all the information related with the images to train. 

The app is in the folder [web_app](web_app)

The codes used to make a trainining dataset is here [build_trainingdatasets](build_trainingdatasets)

The codes used to configure Firebase, upload the training datasets and download the results is here [firebase_manager](firebase_manager)

Along the code are needed the Firebase credentials that have been anonimized. 


# Tutorial

## A little introduction

The webapp needs a training dataset that is stored in a firebase service.
Then before you try to use the app you need to create a training dataset. You need some images to train well ordered and you will need to be very careful to create a description of the dataset to train.

You will think (probably) that all this sometimes is redundant, but working with Firebase needs to precalculate some fields or some global agregations that can be impossible ore tiem consuming.

The web app is developed using VUE2.js

To upload and download data from firebase you will need to use the python scripts. 

Remember that all the steps of the project needs the credentials of firebase that are not included in this repo. So, expect a lot of errors if you don't set it properly.

## Make a training datatset

The training dataset consists in a json file with this structure:

    [
        {
            "vid": "431_1071",
            "votes": 0
        },
        {
            "vid": "1009_1397",
            "votes": 0
        },
        ...
    ]
The "vid" field is the result of the union of two ids, the first and the second element of the image comparision. Put this images in a folder, named with this id.

The "votes" field is 0 by default. After the training it will increase.

Each json file, that describes the pair of images to train, needs to be uploaded to the Firebase project using **addSequence(path, name)** 

After all the sequences of photos to train is uploaded you will need to declare wich are the datasets to train using **addSessions()** and you sill need to change this configurataion.

    c_sessions_open={
            'o1': {'id': 'o1',
                'folder': 'satelite',
                'name':  'small Pablo',
                'description': 'random training for an small part of dataset of satelital imagery',
                'type':'random_subset', // type of training (we expect to have more types)
                'subset':'satx1',
                'seq': 77, // total amount of photos to train
                'votes':0, // current votes (0 at the begining)
                'num_tests':(77*77-77)/2, // optimal number of total votes
                'total_photos':77, // total amount of photos to train
                'limit': -1, //if there's a maximum of votes per photo put here, if not -1
                },
        }

## Upload the data and set firebase.

Here **firebase_manager/fb_manager.py** you will find the example about how to set a firebase server. Remember to set the credentials, and change the dicts used for the configurations according your neeeds.

You will need to execute the function **addSessions()**
Is in this step wher yo define if a sessision of training is private or public.

## Install the app

The app can be installed in a simple managed server. You don't will need php, python or other special dependencies.
Only upload to you server and ready.

In the folder credenttilas/ you will need to replace de credentials.js by a file with your configuration.

The app has two sides, a public page and a manager page. This seconde one is for the workshops where the manager can invite, open, close sessions. If there's no manage the app let's you train any of the open datasets.

### **index.html** : where you will access as a normal user
- you have a first screen to register the user
- you can choose the dataset to train
- if the manager is online can invite you to train a private dataset

landing page
![Alt text](<images/Screenshot from 2024-04-02 11-40-49.png>)
login page
![Alt text](<images/Screenshot from 2024-04-02 11-40-57.png>)

training example
![Alt text](<images/Screenshot from 2024-04-02 11-41-03.png>)
![Alt text](<images/Screenshot from 2024-04-02 11-41-10.png>)

training example
![Alt text](<images/Screenshot from 2024-04-02 11-42-44.png>)
![Alt text](<images/Screenshot from 2024-04-02 11-42-21.png>)


### **control.html** : where you will access as manager. Here you can accept the users, send messages to them and coordinate the participatory process. Only for the private sessions.

![Alt text](<images/Screenshot from 2024-04-02 11-45-07.png>)
![Alt text](<images/Screenshot from 2024-04-02 11-46-02.png>)
![Alt text](<images/Screenshot from 2024-04-02 11-47-00.png>)



## Download the data

Here **firebase_manager/fb_getdata.py** is described the process to downlodad all the votes and store them in sqlite files. 
